package me.psychedelicpelican.rpgmobot;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {

    // Menu components
    Button runScriptButton, stopScriptButton;
    ComboBox scriptsDropDown;
    TextField collectionTimeBox, skillLevelBox;
    Text scripts, collectionTime, skillLevel, current, status, seconds, loop;

    @Override
    public void start(Stage primaryStage) throws Exception{
        // Import classes
        final Bot bot = new Bot(this);
        Controller controller = new Controller(this, bot);

        // Start stuff
        bot.start();

        // Set title
        primaryStage.setTitle("RPG MO Scripts");

        // Make a new "menu" layout
        GridPane menu = new GridPane();

        // Run Script button
        runScriptButton = new Button("Run Script");
        runScriptButton.setOnAction(controller);

        // Stop Script button
        stopScriptButton = new Button("Stop Script");
        stopScriptButton.setOnAction(controller);

        // Scripts text
        scripts = new Text("Scripts: ");

        // Scripts drop down
        ObservableList<String> scriptsList =
                FXCollections.observableArrayList(
                        "Cut Fir Log",
                        "Mine Iron Ore"
                );

        scriptsDropDown = new ComboBox(scriptsList);
        scriptsDropDown.setVisibleRowCount(5);

        // Collection time text
        collectionTime = new Text("Collection Time: ");

        // Collection time box
        collectionTimeBox = new TextField("180");

        // Skill level text
        skillLevel = new Text("Skill Level: ");

        //Skill level box
        skillLevelBox = new TextField("1");

        // Current text
        current = new Text("No Script Running");

        // Status text
        status = new Text("Status: None");

        // Seconds text
        seconds = new Text("Seconds Left: 0");

        // Loop text
        loop = new Text("Loop: 1");

        // Add all components to the menu
        menu.add(runScriptButton, 0, 0);
        menu.add(stopScriptButton, 1, 0);

        menu.add(scripts, 0, 1);
        menu.add(scriptsDropDown, 1, 1);

        menu.add(collectionTime, 0, 2);
        menu.add(collectionTimeBox, 1, 2);

        menu.add(skillLevel, 0, 3);
        menu.add(skillLevelBox, 1, 3);

        menu.add(current, 0, 4);
        menu.add(status, 0, 5);
        menu.add(seconds, 0, 6);
        menu.add(loop, 0, 7);

        // Adjust menu
        menu.setAlignment(Pos.CENTER);
        menu.setHgap(10);
        menu.setVgap(10);
        menu.setPadding(new Insets(25, 25, 25, 25));

        primaryStage.setScene(new Scene(menu, 450, 300));
        primaryStage.show();
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                bot.interrupt();
                Platform.exit();
                System.exit(0);
            }
        });
    }


    public static void main(String[] args) {
        launch(args);
    }
}
