package me.psychedelicpelican.rpgmobot;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by Caidan Williams on 6/20/2016.
 */
public class Scripts {

    private Main main;
    private Robot robot;

    private int lcCutFirLog = 0, lcMineIronOre = 0;
    private int collectionTime;

    public Scripts(Main main, Robot robot) {
        this.main = main;
        this.robot = robot;
    }

    public void CutFirLog() {
        // Get script time
        collectionTime = estimateCollectionTime(24.0, getSkillLevel(), 1.0);

        // Give player time to get game in focus
        setCurrent("3 seconds til the script runs!");
        robot.delay(1000);
        setCurrent("2 seconds til the script runs!");
        robot.delay(1000);
        setCurrent("1 second  til the script runs!");
        robot.delay(1000);
        setCurrent("Script Running");

        // Loop count and increase it
        lcCutFirLog++;
        setLoop(String.valueOf(lcCutFirLog));

        // Movement loop
        setStatus("Finding");
        goSouth(10);

        setStatus("Collecting");
        collectionTimer(collectionTime, 1);

        setStatus("Returning");
        goNorth(10);

        setStatus("Depositing");
        depositAllInChest();
    }

    public void MineIronOre() {
        // Get script time
        collectionTime = getCollectionTime();

        if (collectionTime == -1) {
            return;
        }

        // Give player time to get game in focus
        setCurrent("3 seconds til the script runs!");
        robot.delay(1000);
        setCurrent("2 seconds til the script runs!");
        robot.delay(1000);
        setCurrent("1 second  til the script runs!");
        robot.delay(1000);
        setCurrent("Script Running");

        // Loop count and increment it
        lcMineIronOre++;
        setLoop(String.valueOf(lcMineIronOre));

        // Movement loop
        setStatus("Finding");
        goSouth(4);
        goEast(17);
        goSouth(4);
        robot.delay(2000);
        goWest(1);
        goSouth(6);
        goWest(1);
        goSouth(8);
        goEast(2);
        goSouth(1);

        setStatus("Collecting");
        collectionTimer(getCollectionTime(), 1);

        setStatus("Returning");
        goWest(2);
        goNorth(10);
        goEast(1);
        goNorth(4);
        goEast(2);
        robot.delay(2000);
        goNorth(1);
        goEast(1);
        goNorth(3);
        goWest(17);
        goNorth(5);

        setStatus("Depositing");
        depositAllInChest();
    }


    // All methods to help reduce repetition

    private void depositAllInChest() {
        robot.delay(200);
        robot.keyPress(KeyEvent.VK_E);
        robot.delay(150);
        robot.keyRelease(KeyEvent.VK_E);
    }

    private void collectionTimer(int seconds, int interval) {
        int millis = seconds * 1000;
        for (int i = millis; i > 0; i -= interval * 1000) {
            setSeconds(String.valueOf(i/1000));
            robot.delay(interval * 1000);
        }
    }

    // Move north a given amount
    private void goNorth(int amount) {
        // Move direction using keyboard
        for (int i = 0; i < amount; i++) {
            robot.delay(200);
            robot.keyPress(KeyEvent.VK_W);
            robot.delay(150);
            robot.keyRelease(KeyEvent.VK_W);
        }
    }

    // Move south a given amount
    private void goSouth(int amount) {
        // Move direction using keyboard
        for (int i = 0; i < amount; i++) {
            robot.delay(200);
            robot.keyPress(KeyEvent.VK_S);
            robot.delay(150);
            robot.keyRelease(KeyEvent.VK_S);
        }
    }

    // Move east a given amount
    private void goEast(int amount) {
        // Move direction using keyboard
        for (int i = 0; i < amount; i++) {
            robot.delay(200);
            robot.keyPress(KeyEvent.VK_A);
            robot.delay(150);
            robot.keyRelease(KeyEvent.VK_A);
        }
    }

    // Move west a given amount
    private void goWest(int amount) {
        // Move direction using keyboard
        for (int i = 0; i < amount; i++) {
            robot.delay(200);
            robot.keyPress(KeyEvent.VK_D);
            robot.delay(150);
            robot.keyRelease(KeyEvent.VK_D);
        }
    }

    // Set status text
    private void setStatus(String text) {
        main.status.setText("Status: " + text);
    }

    // Set status text
    private void setCurrent(String text) {
        main.status.setText(text);
    }

    // Set status text
    private void setSeconds(String text) {
        main.status.setText("Seconds Left: " + text);
    }

    // Set status text
    private void setLoop(String text) {
        main.status.setText("Loop: " + text);
    }

    // Get Collection time
    private int getCollectionTime() {
        int time;

        try {
            time = Integer.valueOf(main.collectionTimeBox.getText());
        } catch (NumberFormatException e) {
            setCurrent("Collection Time\nmust be a number!");
            time = -1;
        }
        return time;
    }

    private int estimateCollectionTime(double bsr, double skillLevel, double resourceLevel) {
        double percent = (bsr / 100) + ((skillLevel - resourceLevel) / 100);
        int time = (int) (100 / percent);
        return time;
    }

    // Get Collection time
    private double getSkillLevel() {
        double level;

        try {
            level = Double.valueOf(main.skillLevelBox.getText());
        } catch (NumberFormatException e) {
            setCurrent("Skill Level\nmust be a number!");
            level = 1.0;
        }
        return level;
    }

}
