package me.psychedelicpelican.rpgmobot;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class Controller implements EventHandler<ActionEvent> {

    private Main main;
    private Bot bot;

    public Controller(Main main, Bot bot) {
        this.main = main;
        this.bot = bot;
    }

    @Override
    public void handle(ActionEvent event) {
        // Find where the even is coming from
        Object source = event.getSource();

        // From 'Run Script' button
        if (source.equals(main.runScriptButton)) {
            if (bot.isAlive()) {
                bot.startScriptRunning();
                main.current.setText("Script started!");
            }
        }

        // From 'Stop Script' button
        if (source.equals(main.stopScriptButton)) {
            if (bot.isAlive()) {
                bot.stopScriptRunning();
                main.current.setText("Script will stop\nat the end\nof the loop");
            }
        }
    }
}
