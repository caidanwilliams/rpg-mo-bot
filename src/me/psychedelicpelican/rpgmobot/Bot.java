package me.psychedelicpelican.rpgmobot;

import java.awt.*;

/**
 * Created by Caidan Williams on 6/17/2016.
 */
public class Bot extends Thread {

    // Bot stuff
    private Main main;
    private Robot robot;
    private Scripts scriptsList;

    private boolean scriptRunning;
    public String script = "";

    public Bot(Main main) {
        this.main = main;
    }

    public void run() {
        // Attempt at starting a new Bot
        try {
            robot = new Robot();
            scriptsList = new Scripts(main, robot);
            scriptRunning = false;
        } catch (AWTException e) {
            e.printStackTrace();
            main.current.setText("Please close and reopen\nthe program, and error\nhas occurred!");
        }

        while (true) {
            // Check for interruption
            if (this.isInterrupted()) {
                return;
            }

            if (scriptRunning) {
                // Check for script to run
                try {
                    script = main.scriptsDropDown.getValue().toString();
                } catch (NullPointerException e) {
                    main.current.setText("Please select a\nscript from the menu");
                    script = "";
                }

                // Run checked script
                if (script.equals("Cut Fir Log")) {
                    scriptsList.CutFirLog();
                }
                else if(script.equals("Mine Iron Ore")) {
                    scriptsList.MineIronOre();
                }
                else {
                    scriptRunning = false;
                }
            }
        }
    }

    public void startScriptRunning() {
        scriptRunning = true;
    }

    public void stopScriptRunning() {
        scriptRunning = false;
    }

}
